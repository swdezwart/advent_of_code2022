use std::collections::{HashSet, VecDeque};
use std::fs;

fn main() {
    let input = read_input();
    let mut quickest_paths = vec![vec![usize::MAX; input[0].len()]; input.len()];
    fill_quickest_paths(&input, &mut quickest_paths);
    let answer_a = answer_a(&input, &quickest_paths);
    println!("Answer a is {answer_a}");
    let answer_b = answer_b(&input, &quickest_paths);
    println!("Answer b is {answer_b}");
}

fn fill_quickest_paths(input: &Vec<String>, quickest_paths: &mut Vec<Vec<usize>>) {
    let e_pos = get_pos_of_char(input, 'E').unwrap();
    quickest_paths[e_pos.0][e_pos.1] = 0;
    explore_around(&input, quickest_paths, e_pos);
}

fn answer_a(input: &Vec<String>, quickest_paths: &Vec<Vec<usize>>) -> String {
    let s_pos = get_pos_of_char(input, 'S').unwrap();
    return quickest_paths[s_pos.0][s_pos.1].to_string();
}

fn answer_b(data: &Vec<String>, quickest_paths: &Vec<Vec<usize>>) -> String {
    let mut fewest = usize::MAX;
    for y in 0..data.len() {
        for (x, ch) in data[y].chars().enumerate() {
            if ch == 'a' && fewest > quickest_paths[y][x] {
                fewest = quickest_paths[y][x];
            }
        }
    }
    return fewest.to_string();
}

fn explore_around(data: &Vec<String>, quickest_paths: &mut Vec<Vec<usize>>, pos: (usize, usize)) {
    if pos.0 != 0 {
        let new_pos = (pos.0 - 1, pos.1);
        let from_char = data[pos.0].chars().nth(pos.1).unwrap();
        let to_char = data[new_pos.0].chars().nth(new_pos.1).unwrap();
        if can_move_to(from_char, to_char) && quickest_paths[new_pos.0][new_pos.1] > quickest_paths[pos.0][pos.1] + 1 {
            quickest_paths[new_pos.0][new_pos.1] = quickest_paths[pos.0][pos.1] + 1;
            explore_around(data, quickest_paths, new_pos);
        }
    }
    if pos.0 + 1 < data.len() {
        let new_pos = (pos.0 + 1, pos.1);
        let from_char = data[pos.0].chars().nth(pos.1).unwrap();
        let to_char = data[new_pos.0].chars().nth(new_pos.1).unwrap();
        if can_move_to(from_char, to_char) && quickest_paths[new_pos.0][new_pos.1] > quickest_paths[pos.0][pos.1] + 1 {
            quickest_paths[new_pos.0][new_pos.1] = quickest_paths[pos.0][pos.1] + 1;
            explore_around(data, quickest_paths, new_pos);
        }
    }
    if pos.1 + 1 < data[0].len() {
        let new_pos = (pos.0, pos.1 + 1);
        let from_char = data[pos.0].chars().nth(pos.1).unwrap();
        let to_char = data[new_pos.0].chars().nth(new_pos.1).unwrap();
        if can_move_to(from_char, to_char) && quickest_paths[new_pos.0][new_pos.1] > quickest_paths[pos.0][pos.1] + 1 {
            quickest_paths[new_pos.0][new_pos.1] = quickest_paths[pos.0][pos.1] + 1;
            explore_around(data, quickest_paths, new_pos);
        }
    }
    if pos.1 != 0 {
        let new_pos = (pos.0, pos.1 - 1);
        let from_char = data[pos.0].chars().nth(pos.1).unwrap();
        let to_char = data[new_pos.0].chars().nth(new_pos.1).unwrap();
        if can_move_to(from_char, to_char) && quickest_paths[new_pos.0][new_pos.1] > quickest_paths[pos.0][pos.1] + 1 {
            quickest_paths[new_pos.0][new_pos.1] = quickest_paths[pos.0][pos.1] + 1;
            explore_around(data, quickest_paths, new_pos);
        }
    }
}

fn can_move_to(mut from: char, mut to: char) -> bool {
    if to == 'S' {
        to = 'a';
    }
    if from == 'E' {
        from = 'z';
    }
    if from < to {
        return true;
    }
    if to as usize == from as usize - 1 || to == from {
        return true;
    }
    return false;
}


fn get_pos_of_char(data: &Vec<String>, c: char) -> Option<(usize, usize)> {
    for y in 0..data.len() {
        for (x, ch) in data[y].chars().enumerate() {
            if ch == c {
                return Some((y, x));
            }
        }
    }
    return None;
}

fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
