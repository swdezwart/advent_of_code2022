use std::fs;
use std::collections::HashMap;

const SCORE: [usize; 3] = [3, 0, 6];

fn main() {
    let input = read_input();
    let answer_a = answer_a(&input);
    println!("Answer a is {answer_a}");
    let answer_b = answer_b(&input);
    println!("Answer b is {answer_b}");
}

fn answer_a(input: &Vec<String>) -> String {
    let mut total_sum = 0;
    for line in input {
        let half_size = line.len() / 2;
        let (c1, c2) = line.split_at(half_size);
        for c in c1.chars() {
            if (c2.contains(c)) {
                total_sum += get_priority(c);
                break;
            }
        }
    }
    return total_sum.to_string();
}

fn answer_b(input: &Vec<String>) -> String {
    let mut total_sum = 0;
    for i in (0..input.len()).step_by(3) {
        let mut found = false;
        for c in input[i].chars() {
            let mut occurence = 1;
            for j in (1..3) {
                if (input[i + j].contains(c)) {
                    if occurence == 2 {
                        total_sum += get_priority(c);
                        found = true;
                        break;
                    }
                    occurence += 1;
                }
            }
            if (found) {
                break;
            }
        }
    }
    return total_sum.to_string();
}

fn get_priority(c: char) -> usize
{
    if ((c as usize) < 97) {
        return c as usize - 'A' as usize + 27;
    } else {
        return c as usize - 'a' as usize + 1;
    }
}

fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
