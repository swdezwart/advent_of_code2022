use std::fs;

fn main() {
    let input = read_input();
    let day1a_answer = day1a(&input);
    println!("The answer to day1a is {day1a_answer}");
    let day1b_answer = day1b(&input);
    println!("The answer to day1b is {day1b_answer}");
}

fn day1a(input: &Vec<String>) -> String {
    let mut max_sum = 0;
    let mut current_sum = 0;
    for line in input {
        if line.is_empty() {
            if current_sum > max_sum {
                max_sum = current_sum;
            }
            current_sum = 0;
        } else {
            let current_value = line.parse::<i32>().unwrap();
            current_sum += current_value;
        }
    }
    return max_sum.to_string();
}

fn day1b(input: &Vec<String>) -> String {
    let mut current_sum = 0;
    let mut sums = Vec::new();
    for line in input {
        if line.is_empty() {
            sums.push(current_sum);
            current_sum = 0;
        } else {
            let current_value = line.parse::<i32>().unwrap();
            current_sum += current_value;
        }
    }
    sums.sort_by(|a,b| b.cmp(a));
    let result: i32 = sums[0..3].iter().sum();
    return result.to_string();
}

fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("/home/sjouke/personal_projects/advent_of_code2022/day1/input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
