use std::fs;
use std::collections::HashMap;

fn main() {
    let input = read_input();
    let answer_a = answer_a(&input);
    println!("Answer a is {answer_a}");
    let answer_b = answer_b(&input);
    println!("Answer b is {answer_b}");
}

fn answer_a(input: &Vec<String>) -> String {
    let mut total_sum = 0;
    for line in input {
        let pairs: Vec<&str> = line.split(',').collect();
        let first_range = Range::from_string(pairs[0]);
        let second_range = Range::from_string(pairs[1]);
        if first_range.is_inside(&second_range) || second_range.is_inside(&first_range) {
            total_sum += 1;
        }
    }
    return total_sum.to_string();
}

fn answer_b(input: &Vec<String>) -> String {
    let mut total_sum = 0;
    for line in input {
        let pairs: Vec<&str> = line.split(',').collect();
        let first_range = Range::from_string(pairs[0]);
        let second_range = Range::from_string(pairs[1]);
        if first_range.overlap(&second_range) {
            total_sum += 1;
        }
    }
    return total_sum.to_string();
}

struct Range {
    left: i32,
    right: i32,
}

impl Range {
    fn from_string(str: &str) -> Self {
        let left_right: Vec<&str> = str.split('-').collect();
        Self { left: left_right[0].parse().unwrap(), right: left_right[1].parse().unwrap() }
    }
    fn is_inside(&self, other: &Range) -> bool {
        other.left <= self.left && other.right >= self.right
    }
    fn overlap(&self, other: &Range) -> bool {
        other.left <= self.right && other.right >= self.left
    }
}

fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
