use std::collections::{HashSet, VecDeque};
use std::fs;

fn main() {
    let input = read_input();
    let answer_a = answer_a(&input[0]);
    println!("Answer a is {answer_a}");
    let answer_b = answer_b(&input[0]);
    println!("Answer b is {answer_b}");
}

fn answer_a(input: &String) -> String {
    for i in 4..input.len() {
        if are_unique(&input[i - 4..i]) {
            return i.to_string();
        }
    }
    return "".to_owned();
}

fn answer_b(input: &String) -> String {
    for i in 14..input.len() {
        if are_unique(&input[i - 14..i]) {
            return i.to_string();
        }
    }
    return "".to_owned();
}

fn are_unique(input: &str) -> bool {
    let mut occurence: HashSet<char> = HashSet::new();
    for c in input.chars() {
        if occurence.contains(&c) {
            return false;
        }
        occurence.insert(c);
    }
    return true;
}


fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
