use std::collections::VecDeque;
use std::fs;

fn main() {
    let input = read_input();
    let (stacks, moves) = get_stacks_and_moves_from_input(&input);
    let answer_a = answer_a(&stacks, &moves);
    println!("Answer a is {answer_a}");
    let answer_b = answer_b(&stacks, &moves);
    println!("Answer b is {answer_b}");
}

fn answer_a(stack: &Vec<VecDeque<char>>, moves: &Vec<Move>) -> String {
    let mut result = String::from("");
    let mut stack_clone = stack.clone();
    for m in moves {
        for _ in 0..m.quantity {
            let buf = stack_clone[m.from].pop_back().unwrap();
            stack_clone[m.to].push_back(buf);
        }
    }
    for s in stack_clone {
        result.push(s.back().unwrap().to_owned());
    }
    return result;
}

fn answer_b(stack: &Vec<VecDeque<char>>, moves: &Vec<Move>) -> String {
    let mut result = String::from("");
    let mut stack_clone = stack.clone();
    for m in moves {
        let mut picked = Vec::new();
        for _ in 0..m.quantity {
            picked.push(stack_clone[m.from].pop_back().unwrap());
        }
        for _ in 0..picked.len() {
            stack_clone[m.to].push_back(picked.pop().unwrap());
        }
    }
    for s in stack_clone {
        result.push(s.back().unwrap().to_owned());
    }
    return result;
}

fn get_stacks_and_moves_from_input(input: &Vec<String>) -> (Vec<VecDeque<char>>, Vec<Move>) {
    let mut is_moves = false;
    let number_of_stacks = 9;
    let mut stack: Vec<VecDeque<char>> = vec![VecDeque::new(); number_of_stacks];
    let mut moves = Vec::new();
    for i in 0..input.len() {
        if input[i].is_empty() {
            is_moves = true;
            continue;
        }
        if is_moves {
            let split: Vec<&str> = input[i].split(" ").collect();
            let quantity: usize = split[1].parse().unwrap();
            let from: usize = split[3].parse().unwrap();
            let to: usize = split[5].parse().unwrap();
            moves.push(Move { quantity, from: from - 1, to: to - 1 });
        } else {
            for (j, c) in input[i].chars().enumerate() {
                if c.is_alphabetic() {
                    stack[j / 4].push_front(c);
                }
            }
        }
    }

    return (stack, moves);
}

struct Move {
    quantity: usize,
    from: usize,
    to: usize,
}

fn read_input() -> Vec<String> {
    let contents = fs::read_to_string("input").expect("Cant read");
    return contents.lines().map(ToOwned::to_owned).collect();
}
